## Respostas

### 1 - 
![](./img/desafio-1.png)

### 2 - 

Crio uma chave SSH com o comando`ssh-keygen` e depois configuro a chave pública no gitlab, indo no ícone do usuário, depois em `Preferences > SSH keys`. Após isso posso fazer o `git clone git@gitlab.com:formando-devops-stenio/desafio-gitlab.git` do repositório.

![](./img/desafio-2.png)

### 3 - 

Faço a cópia de todos os arquivos na pasta usando o comando `cp -r * ~/desafio-gitlab/`, adiciono os novos arquivos com `git add *` e faço o commit com `git commit`

### 4 - 

Uso o comando `git push origin main` para fazer o push dos arquivos adicionados no repositório local pelo commit.

### 5 - 

Altero o texto do index.html usando o vim

### 6 - 
Uso o comando `git checkout -b feature` para criar a nova branch, e após isso altero o texto.

### 7 -

Crio o arquivo `.gitlab-ci.yml` baseado no template de página html simples que está na documentação do gitlab. 

```yml
## Usando template para página html simples
pages:
  stage: deploy
  environment: production
  script:
    - echo "Página html já criada"
  artifacts:
    paths:
      - public
```
Após subir fazer o commit e fazer o push com `git push origin feature`, o arquivo irá rodar o pipeline e disponibilizar a página html.

### 8 - 

Alterno novamente para a branch main com `git checkout main` e depois faço o merge com `git merge feature` e para finalizar faço o push do main.

### 9 -

Na GUI do gitlab, indo em `settings > pages` encontro a minha página.

### 10 - 
![](./img/desafio-7.png)

